#!/bin/bash

cp -R vim ~/.vim
cp -R vimrc ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
