set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Actual bundles
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'airblade/vim-gitgutter'

Plugin 'Syntastic'
Plugin 'Vimjas/vim-python-pep8-indent'
Plugin 'tComment'
Plugin 'terryma/vim-multiple-cursors'

Plugin 'ap/vim-css-color'

call vundle#end()
filetype plugin indent on

" Syntastic
let g:syntastic_error_symbol='>>'
let g:syntastic_style_error_symbol='>>'
let g:syntastic_warning_symbol='~~'
let g:syntastic_style_warning_symbol='~~'
let g:syntastic_check_on_open=1

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='hybrid'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.notexists = ''
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.spell = ''

" Some settings
set history=50                  " Keep 50 commands 50 patterns in the history
set autoindent                  " After an eol use the previously used indention
set smartindent                 " Do smart autoindenting when starting a new line.
set ruler                       " Display the current cursor position in bottomleft
set clipboard=unnamedplus       " Copy to system clipboard by default
set visualbell                  " Don't beep
set noerrorbells                " Don't beep
set laststatus=2                " Always show the statusbar.
set t_Co=256                    " Set color scheme to 256
set scrolloff=10                " Scroll 10 lines before reaching the bottom
set colorcolumn=81              " Make the 81th colomn red

" Make :wqa case insensitive
if !exists(":W")
    command W w
    command Q q
    command Wq wq
    command WQ wq
    command Wa wa
    command Qa qa
    command Wqa wqa
    command WQa wqa
    command WQA wqa
endif

" Make cycling through buffers easier
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprevious<CR>

if has('autocmd')
    " For all text files set 'textwidth' to 78 characters.
    augroup SetWrapperForTextFiles
        au!
        au BufRead,BufNewFile *.md setlocal tw=80 spell
        au BufRead,BufNewFile *.txt setlocal tw=80 spell
        autocmd FileType text setlocal textwidth=80
        au BufRead,BufNewFile *.tex setlocal tw=80 spell
    augroup END

    " Automatically remove all trailing whitespace from the file before
    " saving.
    augroup RemoveTrailingWhitespace
        au!
        autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif
    augroup END
endif

"ignore *.ext files
set wildignore=*.swp,*.bak,*.pyc,*.class
" Set the color of the ColorColomn to
hi ColorColumn ctermbg=235
" Set the color of the line number color to Grey
hi SpellBad ctermbg=darkred
hi SpellBad cterm=underline

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on

" Persistent undo
set undofile
set undodir=$HOME/.vim/undo
set undolevels=1000
set undoreload=10000


" ------ Archlabs ------
" Sane vim defaults for ArchLabs

" Arch defaults
runtime! archlinux.vim

" system clipboard (requires +clipboard)
set clipboard^=unnamed,unnamedplus

" additional settings
set modeline           " enable vim modelines
set hlsearch           " highlight search items
set incsearch          " searches are performed as you type
set number             " enable line numbers
set confirm            " ask confirmation like save before quit.
set hidden             " set hidden buffers, allowing switch without save
set wildmenu           " Tab completion menu when using command mode
set expandtab          " Tab key inserts spaces not tabs
set softtabstop=4      " spaces to enter for each tab
set shiftwidth=4       " amount of spaces for indentation
set shortmess+=aAcIws  " Hide or shorten certain messages

let g:netrw_altv = 1
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3

" ------ leader mapping ------

let g:mapleader = "\<Space>"

" ------ enable additional features ------

" enable mouse
set mouse=a
if has('mouse_sgr')
    " sgr mouse is better but not every term supports it
    set ttymouse=sgr
endif

" syntax highlighting disabled
" syntax enable

set linebreak breakindent
set list listchars=tab:>>,trail:~

colorscheme 256-grayvim

" midnight, night, or day
" let g:jinx_colors = 'midnight'
"
" try
"     colorscheme jinx
" catch
"     colorscheme slate
" endtry

" Fix highlighting in visual
highlight Visual cterm=reverse ctermbg=16

if $TERM !=? 'linux'
    " set termguicolors

    " true colors in terminals (neovim doesn't need this)
    " if !has('nvim') && !($TERM =~? 'xterm' || &term =~? 'xterm')
    "     let $TERM = 'xterm-256color'
    "     let &term = 'xterm-256color'
    " endif

    if has('multi_byte') && $TERM !=? 'linux'
        set listchars=tab:»»,trail:•
        set fillchars=vert:┃ showbreak=↪
    endif
endif

" change cursor shape for different editing modes, neovim does this by default
if !has('nvim')
    if exists('$TMUX')
        let &t_SI = "\<Esc>Ptmux;\<Esc>\e[5 q\<Esc>\\"
        let &t_SR = "\<Esc>Ptmux;\<Esc>\e[4 q\<Esc>\\"
        let &t_EI = "\<Esc>Ptmux;\<Esc>\e[2 q\<Esc>\\"
    else
        let &t_SI = "\e[6 q"
        let &t_SR = "\e[4 q"
        let &t_EI = "\e[2 q"
    endif
endif

" " ------ commands ------
"
" command! D Explore
" command! R call <SID>ranger()
"
" " ------ basic maps ------
"
" " open ranger as a file chooser using the function below
" nnoremap <leader>r :call <SID>ranger()<CR>
"
" " paste while in insert mode
" inoremap <silent><C-v> <Esc>:set paste<CR>a<C-r>+<Esc>:set nopaste<CR>a
"
" " change windows with ctrl+(hjkl)
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
" nnoremap <C-H> <C-W><C-H>
"
" " alt defaults
" nnoremap 0 ^
" nnoremap Y y$
" nnoremap n nzzzv
" nnoremap N Nzzzv
"
" " re-visual text after changing indent
" vnoremap > >gv
" vnoremap < <gv
"
" " j = gj :: k = gk  while preserving numbered jumps eg. 12j
" nnoremap <buffer><silent><expr>j v:count ? 'j' : 'gj'
" nnoremap <buffer><silent><expr>k v:count ? 'k' : 'gk'
"
" " open a terminal in $PWD
" nnoremap <silent> <Leader>tt :terminal<CR>
"
" " split the window vertically and horizontally
" nnoremap _ <C-W>s<C-W><Down>
" nnoremap <Bar> <C-W>v<C-W><Right>


" ------ autocmd ------

" Reload changes if file changed outside of vim requires autoread
augroup load_changed_file
    autocmd!
    autocmd FocusGained,BufEnter * if mode() !=? 'c' | checktime | endif
    autocmd FileChangedShellPost * echo "Changes loaded from source file"
augroup END

" when quitting a file, save the cursor position
augroup save_cursor_position
    autocmd!
    autocmd BufReadPost * call setpos(".", getpos("'\""))
augroup END


" " ------ adv maps ------
"
" " strip trailing whitespace, ss (strip space)
" nnoremap <silent> <Leader>ss
"     \ :let b:_p = getpos(".") <Bar>
"     \  let b:_s = (@/ != '') ? @/ : '' <Bar>
"     \  %s/\s\+$//e <Bar>
"     \  let @/ = b:_s <Bar>
"     \  nohlsearch <Bar>
"     \  unlet b:_s <Bar>
"     \  call setpos('.', b:_p) <Bar>
"     \  unlet b:_p <CR>
"
" " global replace
" vnoremap <Leader>sw "hy
"     \ :let b:sub = input('global replacement: ') <Bar>
"     \ if b:sub !=? '' <Bar>
"     \   let b:rep = substitute(getreg('h'), '/', '\\/', 'g') <Bar>
"     \   execute '%s/'.b:rep."/".b:sub.'/g' <Bar>
"     \   unlet b:sub b:rep <Bar>
"     \ endif <CR>
" nnoremap <Leader>sw
"     \ :let b:sub = input('global replacement: ') <Bar>
"     \ if b:sub !=? '' <Bar>
"     \   execute "%s/<C-r><C-w>/".b:sub.'/g' <Bar>
"     \   unlet b:sub <Bar>
"     \ endif <CR>
"
" " prompt before each replace
" vnoremap <Leader>cw "hy
"     \ :let b:sub = input('interactive replacement: ') <Bar>
"     \ if b:sub !=? '' <Bar>
"     \   let b:rep = substitute(getreg('h'), '/', '\\/', 'g') <Bar>
"     \   execute '%s/'.b:rep.'/'.b:sub.'/gc' <Bar>
"     \   unlet b:sub b:rep <Bar>
"     \ endif <CR>
"
" nnoremap <Leader>cw
"     \ :let b:sub = input('interactive replacement: ') <Bar>
"     \ if b:sub !=? '' <Bar>
"     \   execute "%s/<C-r><C-w>/".b:sub.'/gc' <Bar>
"     \   unlet b:sub <Bar>
"     \ endif <CR>
"
" " highlight long lines, ll (long lines)
" let w:longlines = matchadd('ColorColumn', '\%'.&textwidth.'v', &textwidth)
" nnoremap <silent> <Leader>ll
"     \ :if exists('w:longlines') <Bar>
"     \   silent! call matchdelete(w:longlines) <Bar>
"     \   echo 'Long line highlighting disabled'
"     \   <Bar> unlet w:longlines <Bar>
"     \ elseif &textwidth > 0 <Bar>
"     \   let w:longlines = matchadd('ColorColumn', '\%'.&textwidth.'v', &textwidth) <Bar>
"     \   echo 'Long line highlighting enabled'
"     \ <Bar> else <Bar>
"     \   let w:longlines = matchadd('ColorColumn', '\%80v', 81) <Bar>
"     \   echo 'Long line highlighting enabled'
"     \ <Bar> endif <CR>

" " local keyword jump
" nnoremap <Leader>fw
"     \ [I:let b:jump = input('Go To: ') <Bar>
"     \ if b:jump !=? '' <Bar>
"     \   execute "normal! ".b:jump."[\t" <Bar>
"     \   unlet b:jump <Bar>
"     \ endif <CR>
"
" " open ranger as a file chooser
" function! <SID>ranger()
"     let l:temp = tempname()
"     execute 'silent !xterm -e ranger --choosefiles='.shellescape(l:temp).' $PWD'
"     if !filereadable(temp)
"         redraw!
"         return
"     endif
"     let l:names = readfile(l:temp)
"     if empty(l:names)
"         redraw!
"         return
"     endif
"     execute 'edit '.fnameescape(l:names[0])
"     for l:name in l:names[1:]
"         execute 'argadd '.fnameescape(l:name)
"     endfor
"     redraw!
" endfunction
